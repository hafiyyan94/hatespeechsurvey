"""hatespeech URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.views.generic import RedirectView

from home.views import login_view, logout_view, home, list_survey, user_administration, paket_administration, \
    fase_administration, category_administration, option_administration
from api import urls as url_api

urlpatterns = [
    path('admin/login/', login_view),
    path('admin/logout/', logout_view),
    path('admin/', admin.site.urls),
    path('', RedirectView.as_view(url='/login', permanent=True)),
    path('login/', login_view),
    path('logout/', logout_view),
    path('home/', home),
    path('api/', include(url_api)),
    path('list_survey/<int:id_paket>', list_survey),
    path('user_administration/', user_administration),
    path('paket_administration/', paket_administration),
    path('fase_administration/', fase_administration),
    path('category_administration/', category_administration),
    path('option_administration/', option_administration),
]
