from django.contrib.auth.models import User, Group

def create_default_user(*args, **kwargs):

    user_group, user_group_is_created = Group.objects.get_or_create(name='user')
    admin_group, admin_group_is_created= Group.objects.get_or_create(name='admin')

    default_user = User.objects.create_user('default-user', 'default-user@annotation.cs.ui.ac.id', '123default')
    default_admin = User.objects.create_user('default-admin', 'default-admin@annotation.cs.ui.ac.id', '123default')
    default_superadmin = User.objects.create_superuser('default-superadmin', 'default-superadmin@annotation.cs.ui.ac.id', '123default', is_superuser=True)

    user_group.user_set.add(default_user)
    admin_group.user_set.add(default_admin)
    admin_group.user_set.add(default_superadmin)