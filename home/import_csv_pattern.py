from .models import TweetData

DEFAULT_DATA = '-'

def tweet_csv_importer_factory(csv_data, paket_instance):

    if not csv_data is None:
        for row in csv_data:

            complete_tweet_data = get_required_data_in_dictionary(row)
            complete_tweet_data.update({'related_paket': paket_instance})

            TweetData(**complete_tweet_data).save()

    else:
        raise ValueError('The columns must be between 1 until 5')

def get_required_data_in_dictionary(row_data):

    tweet_data = {}
    tweet_data.update(get_tweet_text(row_data))
    tweet_data.update(get_tweet_username(row_data))
    tweet_data.update(get_tweet_caption(row_data))
    tweet_data.update(get_tweet_query(row_data))
    tweet_data.update(get_tweet_image_url(row_data))

    return tweet_data

def get_tweet_text(row_data):

    return {'tweet_text': row_data['text']}

def get_tweet_image_url(row_data):

    try:
        return {'tweet_image': row_data['image_url']}
    except Exception:
        return {'tweet_image': DEFAULT_DATA}

def get_tweet_caption(row_data):

    try:
        return {'tweet_caption': row_data['caption']}
    except Exception:
        return {'tweet_caption': DEFAULT_DATA}

def get_tweet_username(row_data):

    try:
        return {'tweet_username': row_data['username']}
    except Exception:
        return {'tweet_username': DEFAULT_DATA}

def get_tweet_query(row_data):

    try:
        return {'tweet_query': row_data['query']}
    except Exception:
        return {'tweet_query': DEFAULT_DATA}

class TweetFromCSVImporter(object):

    def __init__(self, csv_data, paket_instance):

        self.csv_data = csv_data
        self.related_paket = paket_instance
        self.row_counter = 0

    def add_paket_to_kwargs_key(self, kwargs_dict):

        return kwargs_dict.update({'related_paket': self.related_paket})

    def build_kwargs_arg_for_model(self, row_data_from_csv):

        pass

    def execute(self):

        for row in self.csv_data:

            final_kwargs = self.build_kwargs_arg_for_model(row) + self.add_paket_to_kwargs_key()

            TweetData.objects.create(final_kwargs)
            self.row_counter += 1