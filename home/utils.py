from .models import AnnotationForTweet, Paket
from django.contrib.auth.models import Group
from django.contrib import auth
from .forms import PaketForm, CSV_TWEET_FIELDNAME
import chardet
from .import_csv_pattern import tweet_csv_importer_factory
from io import StringIO
import csv


def total_completed_paket(obj):
    return json_total_completed_and_unfinished_paket_for_user(obj)['completed']


def total_unfinished_paket(obj):
    return json_total_completed_and_unfinished_paket_for_user(obj)['unfinished']

def json_total_completed_and_unfinished_paket_for_user(user):

    get_all_paket_related_to_user = Paket.objects.filter(related_user=user)

    total_paket = get_all_paket_related_to_user.count()
    completed_paket = 0

    for paket in get_all_paket_related_to_user:

        completed_paket += 1 if determine_if_paket_is_completed(paket) else 0

    return {'completed': completed_paket, 'unfinished': total_paket - completed_paket}

def determine_if_paket_is_completed(paket):

    return paket.completion == 100

def calculate_total_tweet_categorized_by_a_user(user):

    return AnnotationForTweet.objects.values('related_tweet').distinct().filter(related_tweet__related_paket__related_user=user).count()

auth.models.User.add_to_class('completed', total_completed_paket)
auth.models.User.add_to_class('unfinished', total_unfinished_paket)
auth.models.User.add_to_class('tweet_categorized_count', calculate_total_tweet_categorized_by_a_user)

class RoleCheckStrategy(object):

    @staticmethod
    def check_is_user(user):

        check_result = False
        try:
            check_result = user.groups.filter(name='user').exists()
        except Exception:
            pass

        return check_result

    @staticmethod
    def check_is_admin(user):

        check_result = False
        try:
            is_admin = user.groups.filter(name='admin').exists()

            is_not_superuser = not user.is_superuser
            check_result = is_admin and is_not_superuser
        except Exception:
            pass

        return check_result

    @staticmethod
    def check_is_superadmin(user):

        check_result = False
        try:
            is_admin = user.groups.filter(name='admin').exists()
            check_result = is_admin and user.is_superuser
        except Exception:
            pass

        return check_result

class UserCheckStrategy(RoleCheckStrategy):

    def __init__(self, user):
        super().__init__(user)

    def check(self):

        return self.user

def processing_upload_csv_for_creating_paket(request):

    response_form = PaketForm(request.POST)

    tweet_csv_file = request.FILES[CSV_TWEET_FIELDNAME].open()
    rawdata = tweet_csv_file.read()
    proper_encoding = determine_encoding(rawdata)
    file = rawdata.decode(proper_encoding)
    csv_reader = csv.DictReader(StringIO(file), delimiter=',')
    paket_instance = save_paket_data_from_response_form(response_form)
    tweet_csv_importer_factory(csv_reader, paket_instance)

def determine_encoding(rawdata):

    result = chardet.detect(rawdata)
    return result['encoding']

def save_paket_data_from_response_form(response_form):

    response_form.fields.pop(CSV_TWEET_FIELDNAME)
    return response_form.save()