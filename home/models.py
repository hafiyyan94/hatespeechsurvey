from django.contrib.auth.models import User
from django.db import models
from django.conf import settings
from .middleware import local

class BaseModel(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    creator = models.ForeignKey(User, on_delete=models.PROTECT, related_name="%(class)s_owner")

    def save(self, *args, **kwargs):
        if self.pk is None and hasattr(local, 'user'):
            self.creator = local.user
        return super(BaseModel, self).save(*args, **kwargs)

    class Meta:
        abstract = True

class Fase(BaseModel):

    name = models.CharField(max_length=100)
    description = models.TextField(blank=True)

    def __str__(self):

        return self.name + ' - ' + self.description

class HateCategory(BaseModel):

    TYPED_VALUE = 'TY'
    SINGLE_OPTION = 'SO'
    MULTIPLE_OPTION = 'MO'

    OPTION_TYPE = [(TYPED_VALUE, 'typed_value'),(SINGLE_OPTION, 'single_option'),(MULTIPLE_OPTION, 'multiple_option')]

    name = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    column_name = models.CharField(max_length=60)
    option_type = models.CharField(max_length=2,choices=OPTION_TYPE, default='TY')
    related_fase = models.ManyToManyField(Fase)

    def __str__(self):
        return self.name + ' - ' + self.description

class HateCategoryOption(BaseModel):

    name = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    value = models.CharField(max_length=20)
    related_hate_category = models.ForeignKey(HateCategory, on_delete=models.PROTECT)

    class Meta:
        unique_together = ('value', 'related_hate_category',)

    def __str__(self):
        return self.name + ' - ' + self.description

class Paket(BaseModel):

    UNFINISHED = 'UF'
    SUBMITTED = 'SB'
    APPROVED = 'AP'

    STATUS = [(UNFINISHED, 'Unfinished'),(SUBMITTED, 'Submitted'),(APPROVED, 'Approved')]

    name = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    related_user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT)
    related_fase = models.ForeignKey(Fase, on_delete=models.PROTECT)
    status = models.CharField(max_length=2, choices=STATUS, default='UF')


    def __str__(self):
        return self.name + ' - ' + self.description

    @property
    def completion(self):

        number_of_tweets_related_to_paket = TweetData.objects.filter(related_paket=self).count()

        number_of_category_in_paket = HateCategory.objects.filter(related_fase__paket=self).count()

        number_of_category_being_answered_per_tweet = AnnotationForTweet.objects.values('related_tweet', 'related_category').distinct().filter(related_tweet__related_paket=self).count()

        try:
            count_answered_tweet = (number_of_category_being_answered_per_tweet * 100) / (
                        number_of_tweets_related_to_paket * number_of_category_in_paket)
            return float("{0:.2f}".format(count_answered_tweet))
        except ZeroDivisionError:
            return 0


class TweetData(BaseModel):

    tweet_username = models.CharField(max_length=256, blank=True)
    tweet_text = models.TextField()
    tweet_image = models.TextField(blank=True)
    tweet_query = models.CharField(max_length=50, blank=True)
    tweet_caption = models.TextField(blank=True)
    related_paket = models.ForeignKey(Paket, on_delete=models.PROTECT)

    def __str__(self):
        return str(self.id) + ' - ' + self.related_paket.name

    @property
    def is_answered(self):
        return bool(AnnotationForTweet.objects.get(related_tweet=self))

class AnnotationForTweet(BaseModel):

    related_tweet = models.ForeignKey(TweetData, on_delete=models.PROTECT)
    related_category = models.ForeignKey(HateCategory, on_delete=models.PROTECT, null=True)
    related_option = models.ForeignKey(HateCategoryOption, on_delete=models.PROTECT, null=True)
    value = models.CharField(max_length=60)

    def __str__(self):

        return 'Tweet with Id ' + str(self.related_tweet.id) + ' have been annotated With Category ' + self.related_category.name + ' - ' + self.related_option.name
