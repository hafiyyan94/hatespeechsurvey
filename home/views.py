from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required, user_passes_test
from django.http import HttpResponseRedirect
from django.shortcuts import render

from home.forms import PaketForm, UserAdminForm, OptionForm, CategoryForm, FaseForm
from .models import HateCategory, Paket
from .utils import calculate_total_tweet_categorized_by_a_user, json_total_completed_and_unfinished_paket_for_user, RoleCheckStrategy
from django.core.exceptions import PermissionDenied

# Create your views here.


def only_non_superuser_access(user):
    if user:
        return not user.is_superuser
    return False


def login_view(request):

    if request.user.is_authenticated:
        return HttpResponseRedirect('/home/')

    response_obj = None
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            response_obj = HttpResponseRedirect('/home/')
        else:
            response_obj = HttpResponseRedirect('/login/')
    else:
        response_obj = render(request, 'home/login.html')

    return response_obj


def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/login')


@login_required(login_url='/login')
@user_passes_test(only_non_superuser_access)
def list_survey(request, id_paket):

    this_paket = Paket.objects.get(id=id_paket)

    if request.user != this_paket.related_user:
        return HttpResponseRedirect('/home/')

    category_related_to_paket = HateCategory.objects.filter(related_fase__paket=id_paket)
    category_related_to_paket = category_related_to_paket.values('name', 'column_name', 'option_type')

    try:
        pakets = Paket.objects.filter(related_user=request.user).exclude(this_paket).values()
    except TypeError:
        # exception if user has no paket
        pakets = Paket.objects.filter(related_user=request.user).values()

    for paket in pakets:
        paket['completion'] = Paket.objects.get(id=paket['id']).completion
    pakets = list(pakets)

    completion = this_paket.completion

    try:
        value_per_answer = 100 / (this_paket.tweetdata_set.count() * this_paket.related_fase.hatecategory_set.count())
    except ZeroDivisionError:
        value_per_answer = 0

    response = {
        'id_paket': id_paket,
        'related_column_name': category_related_to_paket,
        'pakets': pakets,
        'completion': completion,
        'value_per_answer': value_per_answer
    }

    return render(request, 'home/list_survey_for_user.html', response)


@login_required(login_url='/login')
def home(request):
    logged_in_user = request.user

    if RoleCheckStrategy.check_is_superadmin(logged_in_user):
        return HttpResponseRedirect('/admin/')
    elif RoleCheckStrategy.check_is_admin(logged_in_user):
        response = {}
        return render(request, 'home/home_admin.html', response)
    elif RoleCheckStrategy.check_is_user(logged_in_user):
        response = json_total_completed_and_unfinished_paket_for_user(logged_in_user)
        response.update({'tweet_categorized': calculate_total_tweet_categorized_by_a_user(logged_in_user)})
        return render(request, 'home/home_user.html', response)
    else:
        raise PermissionDenied

@login_required(login_url='/login')
def user_administration(request):
    logged_in_admin = request.user
    response = {}
    user_admin_form = UserAdminForm()

    if request.method == 'POST':

        user_form_result = UserAdminForm(request.POST)
        inserted = user_form_result.save()
        print(inserted)

    response['userAdminForm'] = user_admin_form

    return render(request, 'home/user_administration_page.html', response)

@login_required(login_url='/login')
def paket_administration(request):
    logged_in_admin = request.user
    response = {}
    paket_form = PaketForm()
    if request.method == 'POST':

        paket_form_result = PaketForm(request.POST)
        print(paket_form_result)

    response['paketForm'] = paket_form
    return render(request, 'home/paket_administration_page.html', response)

@login_required(login_url='/login')
def fase_administration(request):
    logged_in_admin = request.user
    response = {}
    fase_form = FaseForm()
    if request.method == 'POST':

        fase_form_result = FaseForm(request.POST)
        inserted = fase_form_result.save()

    response['faseForm'] = fase_form
    return render(request, 'home/fase_administration_page.html', response)

@login_required(login_url='/login')
def category_administration(request):
    logged_in_admin = request.user
    response = {}
    category_form = CategoryForm()
    if request.method == 'POST':

        category_form_result = CategoryForm(request.POST)
        inserted = category_form_result.save()

    response['categoryForm'] = category_form
    return render(request, 'home/category_administration_page.html', response)

@login_required(login_url='/login')
def option_administration(request):
    logged_in_admin = request.user
    response = {}
    option_form = OptionForm()
    if request.method == 'POST':

        option_form_result = OptionForm(request.POST)
        inserted = option_form_result.save()

    response['optionForm'] = option_form
    return render(request, 'home/option_administration_page.html', response)
