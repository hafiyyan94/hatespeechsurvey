from django.contrib.auth.models import User, Group
from django import forms
from .models import Paket, HateCategoryOption, HateCategory, Fase

CSV_TWEET_FIELDNAME = 'csv_tweet_file'


class PaketForm(forms.ModelForm):
    csv_tweet_file = forms.FileField()

    class Meta:
        model = Paket
        fields = ['name', 'description', 'related_user', 'related_fase', CSV_TWEET_FIELDNAME]

class FaseForm(forms.ModelForm):

    class Meta:
        model = Fase
        fields = ('name', 'description',)

class CategoryForm(forms.ModelForm):

    class Meta:
        model = HateCategory
        fields = ['name', 'description', 'column_name', 'option_type', 'related_fase']

class OptionForm(forms.ModelForm):

    class Meta:
        model = HateCategoryOption
        fields = ['name', 'description', 'value', 'related_hate_category']


class UserAdminForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'password', 'email')
        widgets = {
            'password': forms.PasswordInput()
        }

    def save(self, commit=True):

        inserted_user = None
        user_group, user_group_is_created = Group.objects.get_or_create(name='user')
        if self.is_valid() and commit:
            inserted_user = User.objects.create_user(username=self.cleaned_data['username'], email=self.cleaned_data['email'],
                                                     password=self.cleaned_data['password'],
                                                     first_name=self.cleaned_data['first_name'], last_name=self.cleaned_data['last_name'])
            user_group.user_set.add(inserted_user)

        return inserted_user
