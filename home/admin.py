import csv
import zipfile
from datetime import datetime
from io import BytesIO, StringIO


from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.core.checks import messages
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import path

from api.utils import csv_data_for_exported_paket, create_csv_from_dict
from home.utils import processing_upload_csv_for_creating_paket
from .forms import PaketForm, CSV_TWEET_FIELDNAME
from .import_csv_pattern import tweet_csv_importer_factory
from .models import Fase, HateCategory, HateCategoryOption, Paket, TweetData, AnnotationForTweet

admin.site.unregister(User)


class CustomUserAdmin(UserAdmin):
    model = User
    list_display = (
    'username', 'is_superuser', 'total_completed_paket', 'total_unfinished_paket', 'total_tweet_categorized')
    list_filter = ('username', 'is_superuser')

    def total_completed_paket(self, obj):
        return obj.completed()

    def total_unfinished_paket(self, obj):
        return obj.unfinished()

    def total_tweet_categorized(self, obj):
        return obj.tweet_categorized_count()

admin.site.register(User, CustomUserAdmin)
# Register your models here.
admin.site.register(Fase)

@admin.register(HateCategory)
class HateCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'option_type', 'used_in_fase', 'column_name')
    list_filter = ('option_type',)
    list_display_links = ('name',)
    list_editable = ('option_type',)
    filter_horizontal = ('related_fase',)

    def used_in_fase(self, obj):

        related_fase_name = []

        for fase in obj.related_fase.all():
            related_fase_name.append(fase.name)

        return ','.join(related_fase_name)

@admin.register(HateCategoryOption)
class HateCategoryOptionAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'value', 'related_hate_category')
    list_filter = ('related_hate_category',)
    list_display_links = ('name',)
    list_editable = ('value',)

@admin.register(TweetData)
class TweetDataAdmin(admin.ModelAdmin):
    list_display = ('id', 'tweet_username', 'tweet_text', 'tweet_query', 'related_paket')
    list_filter = ('related_paket',)
    list_display_links = ('id',)

@admin.register(AnnotationForTweet)
class AnnotationForTweetAdmin(admin.ModelAdmin):
    list_display = ('id','related_tweet', 'related_category', 'related_option')
    list_filter = ('related_tweet', 'related_category', 'related_option')
    list_display_links = ('id',)

@admin.register(Paket)
class PaketAdmin(admin.ModelAdmin):
    actions = ['export_csv']
    list_display = ('name', 'description', 'related_user', 'status', 'completion')
    list_editable = ('status',)
    list_filter = ('status', 'related_user')
    list_display_links = ('name',)

    change_list_template = "customadmin/paket_changelist.html"

    def export_csv(self, request, queryset):

        zipped_file = BytesIO()
        with zipfile.ZipFile(zipped_file, 'w') as f:
            for paket in queryset:
                csv_data_and_header = csv_data_for_exported_paket(paket.id)
                file = create_csv_from_dict(csv_data_and_header)
                f.writestr("{}.csv".format(paket.name), file.getvalue())

        zipped_file.seek(0)
        response = HttpResponse(zipped_file.getvalue(), content_type='application/zip')
        zip_filename = 'exportedPaket' + datetime.now().strftime("%m/%d/%Y")
        response['Content-Disposition'] = 'attachment; filename=\"' + zip_filename + '.zip\"'
        return response

    export_csv.short_description = "Export selected paket to CSV"

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('import-csv/', self.import_csv),
        ]
        return my_urls + urls

    def import_csv(self, request):

        form = PaketForm()
        payload = {"form": form}
        response = render(
            request, "customadmin/paket_importcsv_form.html", payload)

        if request.method == "POST":

            self.processing_import_csv_paket_form(request)
            response = HttpResponseRedirect('/admin/home/paket')

        elif not request.method in ["GET", "POST"]:

            self.message_user(request, "Wrong HTTP Method", level=messages.ERROR)

        return response

    def processing_import_csv_paket_form(self, request):

        processing_upload_csv_for_creating_paket(request)
        self.message_user(request, "Your csv file has been imported")

    def save_paket_data_from_response_form(self, response_form):

        response_form.fields.pop(CSV_TWEET_FIELDNAME)
        return response_form.save()


