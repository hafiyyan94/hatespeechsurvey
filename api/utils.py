import csv
import itertools
from io import StringIO
from io import BytesIO
from django.contrib.auth.models import User
from home.models import HateCategory, HateCategoryOption, TweetData, AnnotationForTweet, Paket, Fase
from django.db.models import Value, CharField
from itertools import chain

ENCODING_DELIMITER = '#'


def list_survey_for_user(id_paket):
    related_tweet_data = TweetData.objects.filter(related_paket__id=id_paket).values()

    category_and_its_options = retrieve_category_and_its_option(id_paket)

    for tweet in related_tweet_data:
        # Assign encoding for new value
        new_options_based_on_category = assign_encoding_to_category_and_options_value(tweet['id'],
                                                                                      category_and_its_options)
        tweet.update(new_options_based_on_category)

    return related_tweet_data


def retrieve_category_and_its_option(id_paket):
    results = {}

    # Retrieve category
    related_hate_category = HateCategory.objects.filter(related_fase__paket__id=id_paket).values()

    # retrieve option
    for category in related_hate_category:
        related_options = HateCategoryOption.objects.filter(related_hate_category__id=category['id']).annotate(
            selected=Value('', output_field=CharField())).values('name', 'selected', 'id', 'value',
                                                                 'related_hate_category')
        results.update({category['column_name']: related_options})

    return results


def assign_encoding_to_category_and_options_value(id_tweet, all_category_and_its_options):
    results = all_category_and_its_options.copy()

    for key, options_per_category in results.items():
        new_options = assign_encoding_to_options_value(id_tweet, options_per_category)
        results.update({key: new_options})

    return results


def assign_encoding_to_options_value(id_tweet, all_options):
    results = []

    for option in all_options:
        id_category = option['related_hate_category']
        id_option = option['id']
        new_options = option.copy()
        new_options.update(
            {'value': create_encoding_for_options_value(id_tweet, option['related_hate_category'], option['id']),
             'selected': is_option_already_selected_for_current_tweet(id_tweet, id_category, id_option)})
        results.append(new_options)

    return results


def is_option_already_selected_for_current_tweet(id_tweet, id_category, id_option):
    try:
        is_selected = AnnotationForTweet.objects.get(related_tweet_id=id_tweet, related_category_id=id_category,
                                                     related_option_id=id_option)
        return 1
    except AnnotationForTweet.DoesNotExist:
        return 0


def create_encoding_for_options_value(id_tweet, hate_category_id, hate_category_option_id):
    return str(id_tweet) + ENCODING_DELIMITER + str(hate_category_id) + ENCODING_DELIMITER + str(
        hate_category_option_id)


def decode_encoding_for_options_value(encoding_value):
    return encoding_value.split(ENCODING_DELIMITER)


def annotating_tweet_logic(id_tweet, id_selected_option, id_category):
    option_type = HateCategory.objects.get(id=id_category).option_type
    annotating_logic = factory_of_annotating_logic(option_type, id_tweet, id_selected_option, id_category)
    return annotating_logic.execute()


def factory_of_annotating_logic(option_type, id_tweet, id_selected_option, id_category):
    if option_type == HateCategory.SINGLE_OPTION:
        return SingleOptionLogic(id_tweet, id_selected_option, id_category)
    elif option_type == HateCategory.MULTIPLE_OPTION:
        return MultipleOptionLogic(id_tweet, id_selected_option, id_category)
    elif option_type == HateCategory.TYPED_VALUE:
        return TypedValueLogic(id_tweet, id_selected_option, id_category)
    else:
        raise ValueError('Invalid Category')


class AnnotatingLogic(object):

    def __init__(self, id_tweet, id_selected_option, id_category):
        self.id_tweet = id_tweet
        self.id_selected_option = id_selected_option
        self.id_category = id_category

    def execute(self):
        pass


class SingleOptionLogic(AnnotatingLogic):

    def __init__(self, id_tweet, id_selected_option, id_category):
        super().__init__(id_tweet, id_selected_option, id_category)
        self.chosen_value = HateCategoryOption.objects.get(id=self.id_selected_option).value

    def execute(self):

        _, is_created = AnnotationForTweet.objects.update_or_create(related_tweet_id=self.id_tweet,
                                                                    related_category_id=self.id_category,
                                                                    defaults={
                                                                        'related_option_id': self.id_selected_option,
                                                                        'value': self.chosen_value})

        if is_created:
            return {
                'detail': 'Success Annotating Tweet id {id_tweet} with Option {id_option}'.format(
                    id_tweet=self.id_tweet, id_option=self.id_selected_option),
                'is_progressing': True
            }
        else:
            return {
                'detail': 'Success Change Annotation for Tweet id {id_tweet} with Option {id_option}'.format(
                    id_tweet=self.id_tweet, id_option=self.id_selected_option),
                'is_progressing': True
            }


class MultipleOptionLogic(AnnotatingLogic):

    def __init__(self, id_tweet, id_selected_option, id_category):
        super().__init__(id_tweet, id_selected_option, id_category)
        self.chosen_value = HateCategoryOption.objects.get(id=self.id_selected_option).value

    def execute(self):

        try:
            AnnotationForTweet.objects.get(related_tweet_id=self.id_tweet, related_option_id=self.id_selected_option,
                                           related_category_id=self.id_category).delete()
            has_answer = bool(AnnotationForTweet.objects.filter(
                related_tweet_id=self.id_tweet, related_category_id=self.id_category
            ))

            return {
                'detail': 'Success Remove Annotation Option {id_option} for Tweet id {id_tweet}'.format(
                    id_tweet=self.id_tweet, id_option=self.id_selected_option),
                'is_progressing': not has_answer
            }

        except AnnotationForTweet.DoesNotExist:

            has_answer = bool(AnnotationForTweet.objects.filter(
                related_tweet_id=self.id_tweet, related_category_id=self.id_category
            ))
            AnnotationForTweet.objects.create(related_option_id=self.id_selected_option,
                                              related_category_id=self.id_category,
                                              value=self.chosen_value, related_tweet_id=self.id_tweet)
            return {
                'detail': 'Success Annotating Tweet id {id_tweet} with Option {id_option}'.format(
                    id_tweet=self.id_tweet, id_option=self.id_selected_option),
                'is_progressing': not has_answer
            }


class TypedValueLogic(AnnotatingLogic):

    def __init__(self, id_tweet, value, id_category):
        super().__init__(id_tweet, value, id_category)

    def execute(self):

        _, is_created = AnnotationForTweet.objects.update_or_create(related_tweet_id=self.id_tweet,
                                                                    related_category_id=self.id_category,
                                                                    defaults={'value': self.id_selected_option})

        if is_created:
            return {
                'detail': 'Success Annotating Tweet id {id_tweet} with Value {id_option}'.format(
                    id_tweet=self.id_tweet, id_option=self.id_selected_option),
                'is_progressing': True
            }
        else:
            return {
                'detail': 'Success Updating Annotation for Tweet id {id_tweet} with Value {id_option}'.format(
                    id_tweet=self.id_tweet, id_option=self.id_selected_option),
                'is_progressing': True
            }


def create_csv_from_dict(dict_data_and_header):
    csv_header = dict_data_and_header['header']
    csv_data = dict_data_and_header['data']
    file = StringIO()

    try:
        writer = csv.DictWriter(file, fieldnames=csv_header)
        writer.writeheader()
        for data in csv_data:
            writer.writerow(data)
        file.seek(0)
    except IOError:
        print("I/O error")

    return file


def csv_data_for_exported_paket(id_paket):
    default_header = ['id', 'username', 'text', 'caption', 'query']
    annotation_data = AnnotationForTweet.objects.values('related_tweet_id', 'related_tweet__tweet_username',
                                                        'related_tweet__tweet_text', 'related_tweet__tweet_query',
                                                        'related_tweet__tweet_caption', 'related_category__column_name',
                                                        'value').filter(related_tweet__related_paket__id=id_paket)
    related_columns = HateCategory.objects.values('column_name').filter(related_fase__paket=id_paket)

    for column in related_columns:
        default_header.append(column['column_name'])
    grouped_annotation_data = create_list_of_dict_from_annotation_data(annotation_data)

    return {'data': grouped_annotation_data, 'header': default_header}


def create_list_of_dict_from_annotation_data(annotation_data):
    results = []
    bridge = itertools.groupby(annotation_data,
                               key=lambda x: (x['related_tweet_id'], x['related_category__column_name']))
    current_tweet_data = -1

    # Create dictionary data
    for key, grp in bridge:
        current_tweet_data = append_dict_data_to_list_return_last_tweet_id_being_focused(results, grp,
                                                                                         current_tweet_data)

    return results


def append_dict_data_to_list_return_last_tweet_id_being_focused(list_container, annotation_data, id_tweet_last_focused):
    last_focused = id_tweet_last_focused
    complete_dict = {}
    grouper_into_list = list(annotation_data)
    if len(grouper_into_list) == 1:

        complete_dict = create_dict_for_single_option(grouper_into_list)

    elif len(grouper_into_list) > 1:

        complete_dict = create_dict_for_multiple_option(grouper_into_list)

    if id_tweet_last_focused == complete_dict['id']:

        # Get the latest element of list container
        updated_annotation_data = list_container[-1]

        options_that_need_to_be_added = set(complete_dict) - set(updated_annotation_data)
        for option in options_that_need_to_be_added:
            updated_annotation_data.update({option: complete_dict[option]})

        list_container[-1] = updated_annotation_data

    else:

        list_container.append(complete_dict)
        last_focused = complete_dict['id']

    return last_focused


def create_dict_for_single_option(annotation_data):
    results = {}
    intended_data = annotation_data[0]
    results['id'] = intended_data['related_tweet_id']
    results['username'] = intended_data['related_tweet__tweet_username']
    results['text'] = intended_data['related_tweet__tweet_text']
    results['caption'] = intended_data['related_tweet__tweet_caption']
    results['query'] = intended_data['related_tweet__tweet_query']
    results.update({intended_data['related_category__column_name']: intended_data['value']})

    return results


def create_dict_for_multiple_option(annotation_data):
    results = {}
    intended_data = annotation_data[0]
    results['id'] = intended_data['related_tweet_id']
    results['username'] = intended_data['related_tweet__tweet_username']
    results['text'] = intended_data['related_tweet__tweet_text']
    results['caption'] = intended_data['related_tweet__tweet_caption']
    results['query'] = intended_data['related_tweet__tweet_query']

    options_selected = []

    for data in annotation_data:
        options_selected.append(data['value'])

    results.update({intended_data['related_category__column_name']: ','.join(options_selected)})

    return results


def submitting_tweet_logic(tweet_id):
    paket = Paket.objects.get(id=tweet_id)

    if paket.completion == 100:
        paket.status = Paket.SUBMITTED
        paket.save()
        return {'detail': 'Paket %s is submitted' % paket.id}

    else:
        return {'detail': 'Paket %s is not finished yet' % paket.id}


def list_pakets_created_by_admin(admin_user_object):
    pakets = Paket.objects.filter(creator=admin_user_object)
    pakets = pakets.values()
    for paket in pakets:
        paket_object = Paket.objects.get(id=paket['id'])
        paket['completion'] = paket_object.completion
        paket['status'] = paket_object.get_status_display()

    return pakets
