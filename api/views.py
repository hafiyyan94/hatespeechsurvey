import json

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User, Group

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from home.models import Paket, Fase, HateCategory, HateCategoryOption, TweetData
from .utils import list_survey_for_user, decode_encoding_for_options_value, annotating_tweet_logic, \
    submitting_tweet_logic, list_pakets_created_by_admin


@api_view(['GET'])
def get_paket_created_by_this_admin(request):
    try:

        return Response(list_pakets_created_by_admin(request.user), status=status.HTTP_200_OK)

    except Paket.DoesNotExist:

        return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['GET'])
def get_fase_created_by_this_admin(request):
    try:

        logged_in_admin = request.user
        list_fase = Fase.objects.values('name', 'description', 'id').filter(creator=logged_in_admin)

        return Response(list_fase, status=status.HTTP_200_OK)

    except Paket.DoesNotExist:

        return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['GET'])
def get_category_created_by_this_admin(request):
    try:

        logged_in_admin = request.user
        list_category = HateCategory.objects.values('name', 'description', 'related_fase', 'column_name', 'option_type',
                                                    'id').filter(creator=logged_in_admin)

        return Response(list_category, status=status.HTTP_200_OK)

    except Paket.DoesNotExist:

        return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['GET'])
def get_option_created_by_this_admin(request):
    try:

        logged_in_admin = request.user
        list_category = HateCategoryOption.objects.values('name', 'description', 'value', 'related_hate_category',
                                                          'id').filter(creator=logged_in_admin)

        return Response(list_category, status=status.HTTP_200_OK)

    except Paket.DoesNotExist:

        return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['GET'])
def get_list_users(request):
    try:
        user_group, _ = Group.objects.get_or_create(name='user')
        list_users = User.objects.filter(groups=user_group).values('username', 'email', 'first_name', 'last_name', 'id')
        return Response(list_users, status=status.HTTP_200_OK)

    except User.DoesNotExist:

        return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['GET'])
def get_list_survey_for_user(request, id_paket):
    try:

        return Response(list_survey_for_user(id_paket), status=status.HTTP_200_OK)

    except Paket.DoesNotExist:

        return Response(status=status.HTTP_404_NOT_FOUND)


@login_required(login_url='/login')
@api_view(['GET'])
def get_pakets(request):
    pakets = Paket.objects.filter(related_user=request.user)
    pakets = pakets.values()
    for paket in pakets:
        paket_object = Paket.objects.get(id=paket['id'])
        paket['completion'] = paket_object.completion
        paket['status'] = paket_object.get_status_display()

    return Response(
        {'pakets': list(pakets)},
        status=status.HTTP_200_OK
    )


@login_required(login_url='/login')
@api_view(['GET'])
def paket_fill(request, paket_id):
    try:
        paket = Paket.objects.get(id=paket_id)
    except Paket.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    tweets = paket.tweetdata_set.all()
    return Response(
        {'tweets': list(tweets)},
        status=status.HTTP_200_OK
    )


@login_required(login_url='/login')
@api_view(['POST'])
def annotate_survey(request):
    try:

        id_tweet, id_category, id_selected_options = decode_encoding_for_options_value(request.POST['value'])
        detail = annotating_tweet_logic(id_tweet, id_selected_options, id_category)

        return Response(detail, status=status.HTTP_200_OK)

    except TweetData.DoesNotExist:

        return Response({'detail': 'Annotated Tweet Not Found'}, status=status.HTTP_404_NOT_FOUND)

    except HateCategoryOption.DoesNotExist:

        return Response({'detail': 'Annotating Option Not Found'}, status=status.HTTP_404_NOT_FOUND)

    except HateCategoryOption.DoesNotExist:

        return Response({'detail': 'Please use key \'Value\' to submit the encoded value'},
                        status=status.HTTP_404_NOT_FOUND)


@login_required(login_url='/login')
@api_view(['POST'])
def submit_survey(request):
    try:
        paket_id = request.POST['value']
        detail = submitting_tweet_logic(paket_id)

        return Response(detail, status=status.HTTP_200_OK)


    except Paket.DoesNotExist:

        return Response({'detail': 'Paket Not Found'}, status=status.HTTP_404_NOT_FOUND)
