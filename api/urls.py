from django.contrib import admin
from django.urls import path
from api import views

urlpatterns = [
    path('paket/', views.get_pakets, name='paket_list'),
    path('list_paket_admin/', views.get_paket_created_by_this_admin, name='paket_list'),
    path('list_fase_admin/', views.get_fase_created_by_this_admin, name='fase_list'),
    path('list_category_admin/', views.get_category_created_by_this_admin, name='category_list'),
    path('list_option_admin/', views.get_option_created_by_this_admin, name='option_list'),
    path('paket/fill/<int:id>', views.paket_fill, name='paket_fill'),
    path('list_survey_based_on_paket/<int:id_paket>', views.get_list_survey_for_user),
    path('annotate_survey/', views.annotate_survey),
    path('submit_survey/', views.submit_survey),
    path('list_users/', views.get_list_users)
]
